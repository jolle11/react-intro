const Text = (props) => (
    <span>
        Random text and {props.userData.name} {props.userData.lastName}
    </span>
);
export default Text;
