// IMPORTS
import { Text, Hability } from '../../components';
import './Header.scss';

const Header = (props) => {
    const { userData, numbers } = props;
    return (
        <header className="header">
            <h2 className="header__title">
                Welcome back, {userData.name}, you are {numbers}
            </h2>
            <Text userData={userData} />
            {userData.habilities.map((hab) => {
                return <Hability hability={hab} />;
            })}
            <p className="header__text">Lorem ipsum dolor sit amet.</p>
        </header>
    );
};
export default Header;
