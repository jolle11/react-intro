import Header from './Header/Header';
import Footer from './Footer/Footer';
import Text from './Text/Text';
import Hability from './Hability/Hability';

export { Header, Footer, Text, Hability };
