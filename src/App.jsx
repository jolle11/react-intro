import { Header, Footer } from './components';
import './App.scss';

function App() {
    const user = {
        name: 'Jordi',
        lastName: 'Olle',
        habilities: ['developer', 'driver', 'chef'],
    };
    return (
        <div className="app">
            <Header userData={user} numbers={11} />
            <div>This is the app content</div>
            <Footer />
        </div>
    );
}

export default App;
